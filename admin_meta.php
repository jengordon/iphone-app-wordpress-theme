<?php
/**
 * Registering meta boxes
 *
 * All the definitions of meta boxes are listed below with comments.
 * Please read them CAREFULLY.
 *
 * You also should read the changelog to know what has been changed before updating.
 *
 * For more information, please visit: 
 * @link http://www.deluxeblogtips.com/2010/04/how-to-create-meta-box-wordpress-post.html
 */

/********************* META BOX DEFINITIONS ***********************/

/**
 * Prefix of meta keys (optional)
 * Use underscore (_) at the beginning to make keys hidden
 * Alt.: You also can make prefix empty to disable it
 */
// Better has an underscore as last sign
$prefix = 'iptemp';
$uploader = get_bloginfo( 'version' ) > "3.3.0" ? 'plupload_image' : 'image';
$uploader_desc = get_bloginfo( 'version' ) > "3.3.0" ? 'Click Edit to add or change desciptions' : 'Click Edit (after upload) for each image to write App name and Url.';

global $meta_boxes;

$meta_boxes = array();

// Email
$meta_boxes[] = array(
	'id'		=> 'contact-form',
	'title'		=> 'Contact Form',
        'context'       => 'side',
	'pages'		=> array( IPTEMP_POST_TYPE ),
    	'priority'      => 'low',
	'fields'	=> array(
                        		array(
			'name'	=> 'Email',
			'id'	=> "{$prefix}-contact-form-email",
			'type'	=> 'text',
			'std'	=> '',
			'desc'	=> 'Enter the email address where you want all contact form inquiries to be sent (leave blank to hide). <br /><br />All email addresses collected with the email subscriber and can be downloaded from <a target="_blank" href="'. home_url(). '/wp-admin/edit.php?post_type=iphone-app-page&get_mail_subscribers=true">here</a>'
		),
		array(
			'name'		=> 'Show Subscribe Form', 
			'id'		=> "{$prefix}-hide-subscribe",
			'type'		=> 'checkbox',
			'desc'		=> 'Toggle subscribe form on/off',			
			'std'		=> 1
		),
        )
);
        
                                                
// Color Scheme
$meta_boxes[] = array(
	'id'		=> 'color-scheme',
	'title'		=> 'Select a Color Scheme',
	'pages'		=> array( IPTEMP_POST_TYPE ),
	'context'       => 'side',
    	'priority'      => 'low',
	'fields'	=> array(
		// Colorscheme
		array(
			'name'      => '',
			'id'        => "{$prefix}-color-scheme-name",
                        'schemes'   => array('black-green', 'black-orange', 'black-red', 'white-green', 'white-orange', 'white-red'),
			'type'      => 'colorscheme',
			'std'       => 'black-green',			
		),
	)
);
                        
// App Details
$meta_boxes[] = array(
	'id'		=> 'app-details',
	'title'		=> 'App Details',
	'pages'		=> array( IPTEMP_POST_TYPE ),
    	'priority'      => 'low',
	'fields'	=> array(
		// Application logo
		array(
			'name'	=> 'App Icon',
			'desc'	=> '',
			'id'	=> "{$prefix}-top-app-icon",
                        'mode'  => 'single',
			'type'	=> 'image',
                        'multiple' => 'false',
                        'desc'     => '<br />Icon image must be 87px x 87px. <a href="' . IPTEMP_URL . '/images/app-icon-large-template.psd.zip" target="_blank">Click here</a> to download the Photoshop template.'
		),
		// Application name
		array(
			'name'	=> 'Application Name',
			'id'	=> "{$prefix}-appname",
			'type'	=> 'text',
			'std'	=> '',
		),
		// Application Tagline
		array(
			'name'	=> 'Application Tagline',
			'id'	=> "{$prefix}-apptagline",
			'type'	=> 'text',
			'std'	=> '',
		),
		// Application Orientation Mode
		array(
			'name'	=> 'Application Orientation Landscape',
			'id'	=> "{$prefix}-applandscape",
			'type'	=> 'checkbox',
			'std'	=> '',
		),
		// Application domain
		array(
			'name'	=> 'Domain mapping',
			'id'	=> "{$prefix}-domain",
			'type'	=> 'text',
			'std'	=> '',
                        'desc'	=> 'Map a domain to this page (myapp.com)',
		),
		// App Price
		array(
			'name'	=> 'Price',
			'id'	=> "{$prefix}-app-price",
			'type'	=> 'text',
			'std'	=> '',
			'desc'	=> 'This is the price that appears on the "Available in the App Store" button. If you app hasn\'t launched, write "Coming Soon"',
                        'size'  => '50'
		),
                // iTunes URL
		array(
			'name'	=> 'iTunes URL',
			'id'	=> "{$prefix}-app-itunes-url",
			'type'	=> 'text',
			'std'	=> '',
			'desc'	=> 'This is where they click to buy the app. If the app isn\'t live yet, just leave this blank and there will be no click-thru URL.',
                        'size'  => '50'
		),
		// Facebook URL
		array(
			'name'	=> 'Facebook URL',
			'id'	=> "{$prefix}-facebook-url",
			'type'	=> 'text',
			'std'	=> '',
                        'size'  => '50'
		),
		// Twitter ID
		array(
			'name'	=> 'Twitter ID',
			'id'	=> "{$prefix}-twitter-id",
			'type'	=> 'text',
			'std'	=> '',
                        'size'  => '50'
		),
		// LinkedIn URL
		array(
			'name'	=> 'LinkedIn URL',
			'id'	=> "{$prefix}-linkedin-url",
			'type'	=> 'text',
			'std'	=> '',
                        'size'  => '50'
		),
	)
);
                        
$meta_boxes[] = array(
	'id'		=> 'headline_body',
	'title'		=> 'Headline & Body',
	'pages'		=> array( IPTEMP_POST_TYPE ),
    	'priority'      => 'low',
	'fields'	=> array(
		// Headline
		array(
			'name'	=> 'Headline',
			'id'	=> "{$prefix}-headline",
			'type'	=> 'text',
			'std'	=> '',
                        'size'  => '50'
		),
		// Body
		array(
			'name'	=> 'Body',
			'id'	=> "{$prefix}-body-text",
			'type'	=> 'wysiwyg',
			'std'	=> '',
		),		
	)
);
                        
                

                        
$meta_boxes[] = array(
	'id'		=> 'email_form',
	'title'		=> 'Email Subscription Form',
	'pages'		=> array( IPTEMP_POST_TYPE ),
    	'priority'      => 'low',
	'fields'	=> array(
		// Headline
		array(
			'name'	=> 'Headline',
			'id'	=> "{$prefix}-email-headline",
			'type'	=> 'text',
			'std'	=> '',
                        'size'  => '50'
		),
		// Body
		array(
			'name'	=> 'Body',
			'id'	=> "{$prefix}-email-body-text",
			'type'	=> 'wysiwyg',
			'std'	=> '',
		),		
	)
);
                        
$meta_boxes[] = array(
	'id'		=> 'screenshots',
	'title'		=> 'Screenshots & Video Features',
	'pages'		=> array( IPTEMP_POST_TYPE ),
    	'priority'      => 'low',
	'fields'	=> array(
		// Application logo
		array(
			'name'	=> 'Screenshots',
			'desc'	=> '<br />' . $uploader_desc,
			'id'	=> "{$prefix}-screenshots",
			'type'	=> $uploader,
                        'mediafields' => array('post_title' => 'Title', 'post_excerpt' => 'Description', 'post_content' => 'Embedded URL')
		),		
	)
);

// Key Features
$meta_boxes[] = array(
	'id'		=> 'key-features',
	'title'		=> 'Key Features',
	'pages'		=> array( IPTEMP_POST_TYPE ),
    	'priority'      => 'low',
	'fields'	=> array(
		// Application logo
		array(
			'name'		=> 'Add Key Features',
			'id'		=> "{$prefix}-key-features",
			'desc'		=> '',	
			'clone'		=> true,
			'type'		=> 'textarea', 
                        'rows'          => '1',
			'std'		=> ''
		),		
	)
);
                        
$meta_boxes[] = array(
	'id'		=> 'user-reviews',
	'title'		=> 'User Reviews',
	'pages'		=> array( IPTEMP_POST_TYPE ),
    	'priority'      => 'low',
	'fields'	=> array(
		// Headline
		array(
			'name'	=> 'Title',
			'id'	=> "{$prefix}-user-reviews-title",
			'type'	=> 'text',
			'std'	=> '',
                        'size'  => '50'
		),
		// Reviews
		array(
			'name'		=> '',
			'id'		=> "{$prefix}-user-reviews",
			'desc'		=> '',	
                        'cols'          => array(
                                                 'main-quote' => 'Main Quote', 
                                                 'body-quote' => 'Body Quote',
                                                 'name' => 'Name',
                                                 'url-text' => 'URL Text',
                                                 'url-link' => 'URL Link'
                                                ),
                                
                        'cols_settings'          => array(                                                 
                                                 'body-quote' => array('type' => 'textarea'),
                                                ),
			'clone'		=> true,
			'type'		=> 'cols',                        
			'std'		=> ''
		),		
	)
);


// About us
$meta_boxes[] = array(
	'id'		=> 'about',
	'title'		=> 'About Us',
	'pages'		=> array( IPTEMP_POST_TYPE ),
    	'priority'      => 'low',
	'fields'	=> array(
		// Headline
		array(
			'name'	=> 'Title',
			'id'	=> "{$prefix}-about-us-title",
			'type'	=> 'text',
			'std'	=> '',
                        'size'  => '50'
		),
		// Body
		array(
			'name'	=> 'Company Bio',
			'id'	=> "{$prefix}-company-bio",
			'type'	=> 'wysiwyg',
			'std'	=> '',
		),
		// Team Members
		array(
			'name'	=> 'Team Members',
			'desc'	=> '<br />' . $uploader_desc,
			'id'	=> "{$prefix}-team-member",
			'type'	=> $uploader,
                        'mediafields' => array('post_title' => 'Name', 'post_excerpt' => 'Role', 'post_content' => 'Description')
		),	
                                
	)
);
                        
// Our Apps
$meta_boxes[] = array(
	'id'		=> 'our-other-apps',
	'title'		=> 'Our other Apps',
	'pages'		=> array( IPTEMP_POST_TYPE ),
    	'priority'      => 'low',
	'fields'	=> array(
		// Team Members
		array(
			'name'	=> 'Our Other Apps',
			'desc'	=> '<br />' . $uploader_desc,
			'id'	=> "{$prefix}-other-apps",
			'type'	=> $uploader,
                        'mediafields' => array('post_title' => 'App Name', 'post_excerpt' => 'Description', 'post_content' => 'App URL')
		),	
                                
	)
);                  


// 1st meta box
                        /*
$meta_boxes[] = array(
	// Meta box id, UNIQUE per meta box
	'id' => 'personal',

	// Meta box title - Will appear at the drag and drop handle bar
	'title' => 'Personal Information',

	// Post types, accept custom post types as well - DEFAULT is array('post'); (optional)
	'pages' => array( IPTEMP_POST_TYPE ),

	// Where the meta box appear: normal (default), advanced, side; optional
	'context' => 'normal',

	// Order of meta box: high (default), low; optional
	'priority' => 'low',

	// List of meta fields
	'fields' => array(
		array(
			// Field name - Will be used as label
			'name'		=> 'Full name',
			// Field ID, i.e. the meta key
			'id'		=> $prefix . 'fname',
			// Field description (optional)
			'desc'		=> 'Format: First Last',
			// CLONES: Add to make the field cloneable (i.e. have multiple value)
			'clone'		=> true,
			'type'		=> 'text',
			// Default value (optional)
			'std'		=> 'Anh Tran'
		),
		array(
			'name'		=> 'Day of Birth',
			'id'		=> "{$prefix}dob",
			'type'		=> 'date',
			// Date format, default yy-mm-dd. Optional. See: http://goo.gl/po8vf
			'format'	=> 'd MM, yy'
		),
		// RADIO BUTTONS
		array(
			'name'		=> 'Gender',
			'id'		=> "{$prefix}gender",
			'type'		=> 'radio',
			// Array of 'key' => 'value' pairs for radio options.
			// Note: the 'key' is stored in meta field, not the 'value'
			'options'	=> array(
				'm'			=> 'Male',
				'f'			=> 'Female'
			),
			'std'		=> 'm',
			'desc'		=> 'Need an explaination?'
		),
		// TEXTAREA
		array(
			'name'		=> 'Bio',
			'desc'		=> "What's your professions? What have you done so far?",
			'id'		=> "{$prefix}bio",
			'type'		=> 'textarea',
			'std'		=> "I'm a special agent from Vietnam.",
			'cols'		=> "40",
			'rows'		=> "8"
		),
		// File type: select box
		array(
			'name'		=> 'Where do you live?',
			'id'		=> "{$prefix}place",
			'type'		=> 'select',
			// Array of 'key' => 'value' pairs for select box
			'options'	=> array(
				'usa'		=> 'USA',
				'vn'		=> 'Vietnam'
			),
			// Select multiple values, optional. Default is false.
			'multiple'	=> true,
			// Default value, can be string (single value) or array (for both single and multiple values)
			'std'		=> array( 'vn' ),
			'desc'		=> 'Select the current place, not in the past'
		),
		array(
			'name'		=> 'About WordPress',    // File type: checkbox
			'id'		=> "{$prefix}love_wp",
			'type'		=> 'checkbox',
			'desc'		=> 'I love WordPress',
			// Value can be 0 or 1
			'std'		=> 1
		),
		// HIDDEN
		array(
			'id'		=> "{$prefix}invisible",
			'type'		=> 'hidden',
			// Hidden field must have predefined value
			'std'		=> "no, i'm visible"
		),
		// PASSWORD
		array(
			'name'		=> 'Your favorite password',
			'id'		=> "{$prefix}pass",
			'type'		=> 'password'
		),
	)
);

// 2nd meta box
$meta_boxes[] = array(
	'id'		=> 'additional',
	'title'		=> 'Additional Information',
	'pages'		=> array( IPTEMP_POST_TYPE ),

	'fields'	=> array(
		// WYSIWYG/RICH TEXT EDITOR
		array(
			'name'	=> 'Your thoughts about Deluxe Blog Tips',
			'id'	=> "{$prefix}thoughts",
			'type'	=> 'wysiwyg',
			'std'	=> sprintf( "%1\$sIt's great!", '<b>', '</b>' ),
			'desc'	=> 'Do you think so?'
		),
		// FILE UPLOAD
		array(
			'name'	=> 'Upload your source code',
			'desc'	=> 'Any modified code, or extending code',
			'id'	=> "{$prefix}code",
			'type'	=> 'file'
		),
		// IMAGE UPLOAD
		array(
			'name'	=> 'Screenshots',
			'desc'	=> 'Screenshots of problems, warnings, etc.',
			'id'	=> "{$prefix}screenshot",
			'type'	=> 'image'
		),
		// NEW(!) PLUPLOAD IMAGE UPLOAD (WP 3.3+)
		array(
			'name'	=> 'Screenshots (plupload)',
			'desc'	=> 'Screenshots of problems, warnings, etc.',
			'id'	=> "{$prefix}screenshot2",
			'type'	=> 'plupload_image'
		)
	)
);

// 3rd meta box
$meta_boxes[] = array(
	'id'		=> 'survey',
	'title'		=> 'Survey',
	'pages'		=> array( IPTEMP_POST_TYPE ),

	'fields'	=> array(
		// COLOR
		array(
			'name'		=> 'Your favorite color',
			'id'		=> "{$prefix}color",
			'type'		=> 'color'
		),
		// CHECKBOX LIST
		array(
			'name'		=> 'Your hobby',
			'id'		=> "{$prefix}hobby",
			'type'		=> 'checkbox_list',
			// Options of checkboxes, in format 'key' => 'value'
			'options'	=> array(
				'reading'	=> 'Books',
				'sport'		=> 'Gym, Boxing'
			),
			'desc'		=> 'What do you do in free time?'
		),
		// TIME
		array(
			'name'		=> 'When do you get up?',
			'id'		=> "{$prefix}getdown",
			'type'		=> 'time',
			// Time format, default hh:mm. Optional. @link See: http://goo.gl/hXHWz
			'format'	=> 'hh:mm:ss'
		),
		// DATETIME
		array(
			'name'		=> 'When were you born?',
			'id'		=> "{$prefix}born_time",
			'type'		=> 'datetime',
			// Time format, default hh:mm. Optional. @link See: http://goo.gl/hXHWz
			'format'	=> 'hh:mm:ss'
		)
	)
);
*/

/********************* META BOX REGISTERING ***********************/

/**
 * Register meta boxes
 *
 * @return void
 */
function iptemp_register_meta_boxes()
{
	global $meta_boxes;

	// Make sure there's no errors when the plugin is deactivated or during upgrade
	if ( class_exists( 'RW_Meta_Box_Iptemp' ) )
	{
		foreach ( $meta_boxes as $meta_box )
		{
			new RW_Meta_Box_Iptemp( $meta_box );
		}
	}
}
// Hook to 'admin_init' to make sure the meta box class is loaded
//  before (in case using the meta box class in another plugin)
// This is also helpful for some conditionals like checking page template, categories, etc.
add_action( 'admin_init', 'iptemp_register_meta_boxes' );