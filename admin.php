<?php
// Includes
require_once( IPTEMP_LIBPATH . '/metabox/metabox.php');
require_once( IPTEMP_PATH . 'admin_meta.php');

// Register activation
register_activation_hook(__FILE__, 'iptemp_plugin_activate');

// Action on init
add_action( 'init', 'iptemp_admin_init');
add_action( 'admin_init', 'mail_subscribers_download');

function iptemp_admin_init()
{        
    add_action( 'admin_menu','iptemp_admin_actions');
    //add_action( 'admin_menu', 'add_some_box');    
    add_action( 'admin_print_scripts', 'iptemp_admin_scripts');
    add_action( 'admin_print_styles', 'iptemp_admin_styles');        
    //add_action( 'save_post', 'smashing_save_post_class_meta', 10, 2 );

    // Add delete action for cache
    iptempCache::init()->addDeleteAction();
}

function iptemp_admin() {
	global $wpdb;
	$db_prefix = $wpdb->prefix; 
	if(!current_user_can('manage_options') || !current_user_can('edit_posts')) {
		echo '<div id="message" class="error">'. __("You don't have permissions to use this plugin","newsletter-generator") .' </div>';
	} else {

	}
}

function iptemp_info() {
	if($_REQUEST['iptemp_info'] == 'true') {
            update_option( 'iphone_app_homepage_id', $_REQUEST['iphone_homepage_id'] );
            update_option( 'iptemp_cache', $_REQUEST['iptemp_cache'] );
        }
	$meta_info = array(
	'id'		=> 'about',
	'title'		=> 'About Us',
	'pages'		=> array( 'info' ),
    	'priority'      => 'low',
	'fields'	=> array(
		// Headline
		array(
			'name'	=> 'Title',
			'id'	=> "{$prefix}-about-us-title",
			'type'	=> 'text',
			'std'	=> '',
                        'size'  => '50'
		),
		// Body
		array(
			'name'	=> 'Company Bio',
			'id'	=> "{$prefix}-company-bio",
			'type'	=> 'wysiwyg',
			'std'	=> '',
		),
		// Team Members
		array(
			'name'	=> 'Team Members',
			'desc'	=> '<br />' . $uploader_desc,
			'id'	=> "{$prefix}-team-member",
			'type'	=> $uploader,
                        'mediafields' => array('post_title' => 'Name', 'post_excerpt' => 'Role', 'post_content' => 'Description')
		),	
                                
	));
        echo '<h3>iPhone App Info</h3>';
        echo '<form method="post" name="iptemp_settings">';
        
        
        echo '<input type="hidden" name="iptemp_info" value="true" />';
        echo '<p><input type="submit" name="submit_form" class="button-primary" value="Update Info" /></p>';
        echo '</form>';
		//print_r($meta_info);
		new RW_Meta_Box_Iptemp( $meta_info );
		
        //add_meta_box('iptemp-about-us','About Us','iptemp_about_us_meta_box',$posttype,'normal','high');
		//do_meta_boxes($posttype,'normal',null);

}
function iptemp_settings() {
        
        if($_REQUEST['iptemp_settings'] == 'true') {
            update_option( 'iphone_app_homepage_id', $_REQUEST['iphone_homepage_id'] );
            update_option( 'iptemp_cache', $_REQUEST['iptemp_cache'] );
        }

        echo '<h3>iPhone App Settings</h3>';
        echo '<form method="post" name="iptemp_settings">';
        
        // Set homepage
        echo '<h4>Set as homepage:</h4><p><select name="iphone_homepage_id"><option value="">None</option>';
        
        $args = array(
                     'post_type' => 'iphone-app-page',
                     'posts_per_page' => -1
                     //'paged' => ( get_query_var('paged') ? get_query_var('paged') : 1),
                     );
        query_posts($args);
        while (have_posts()) : the_post();
        
            if(get_option('iphone_app_homepage_id') == get_the_ID())
              echo '<option value="' . get_the_ID() . '" SELECTED>' . get_the_title() . '</option>';
            else
              echo '<option value="' . get_the_ID() . '">' . get_the_title() . '</option>';
        
        endwhile;
        
        echo '</select></p>';
        echo '<br />';
        // Cache
        echo '<h4>Cache App Pages to HTML:</h4>';
        echo '<small>Creates a prerengenerated HTML-file for your App Page. Improves the page loading up to 8 times.<br />* Requires <a href="http://nginx.org/" target="_blank">Nginx Webserver</a><br /><br />Instructions:<br /> 1. create wp-content/cache and make it writtable<br />2. Update Nginx rewrite with code below</small>';
        echo '<p><label>Yes </label>';
        $checked = get_option('iptemp_cache') ? ' checked ' : '';
        echo '<input type="radio" name="iptemp_cache" value="1" ' . $checked . '> ';
        echo '<label> No </label>';
        $checked = !get_option('iptemp_cache') ? ' checked ' : '';
        echo '<input type="radio" name="iptemp_cache" value="0" ' . $checked . '>';
        
        echo '<br />';
        echo '<br />';
        
        echo '<h4>Nginx Cache Rewrite Rule</h4>';
        echo '<small>Add/change your server section with the code below for your Nginx config.</small>';
        echo '<p><code>
        <pre>
location / {
    set $iptempcache_file /wp-content/cache/iptemp/$http_host/$uri/index.html;
    try_files $iptempcache_file $uri $uri/ /index.php?$args;
}</pre>
        </code></p>';
        
        echo '<input type="hidden" name="iptemp_settings" value="true" />';
        echo '<p><input type="submit" name="submit_form" class="button-primary" value="Update Settings" /></p>';
        echo '</form>';
}

function iptemp_admin_actions() {
	if(iptemp_check_user_permission()) {
            //add_options_page("iPhone App Settings","iPhone App Page Settings",1,"iPhone-App-Settings","iptemp_settings");  
			//add_submenu_page("edit.php?post_type=iphone-app-page","info","Info","manage_options", "iphone-app-page-info", "iptemp_info");  
            add_submenu_page("edit.php?post_type=iphone-app-page","settings","Settings","manage_options", "iphone-app-page-settings", "iptemp_settings");  
	}
}


function iptemp_admin_scripts() {
	if( ( ! empty( $_REQUEST['post_type'] ) && $_REQUEST['post_type'] == "iphone-app-page" ) || !empty( $_REQUEST['post'] ) ) {
		wp_enqueue_script('jqueryagain', IPTEMP_URL . '/js/jquery-min.js');
	}
}

function iptemp_admin_styles() {
	wp_enqueue_style( 'iptemp_admin_css', IPTEMP_URL . '/css/iptemp-style.css' );
}

function iptemp_plugin_activate() {
	global $wpdb;
	$db_prefix = $wpdb->prefix;
}

function iptemp_check_user_permission() {
	if(current_user_can('manage_options') || current_user_can('edit_posts'))
		return true;
	else 
		return false;
}

function iptemp_create_table($table_name, $sql) {
	global $wpdb;
        $db_prefix = $wpdb->prefix; 
	if($wpdb->get_var("show tables like '". $table_name . "'") != $table_name) {
		$wpdb->query($sql);
   }
}

function mail_subscribers_download() {
    
    if(isset($_REQUEST['get_mail_subscribers'])) {        
        $file = ABSPATH . '.email_subscribe_list.txt';

        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($file));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
            exit;
        } else {
            die ("Ops, could not find the file $file?");
        }
        
        die("");    
    }
}


