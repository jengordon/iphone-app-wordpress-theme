<?php
/*
	Plugin Name: iPhone App Wordpress Theme
	Description: Promote your iPhone app with style with this theme from Jen Gordon at <a href="http://www.tapptics.com">www.tapptics.com</a>. Choose from six different color schemes, and customize with screenshots, videos, testimonials and a bio of your company. Plus, collect email address or support questions! For support questions on the theme, please contact Jen by filling out the contact form at <a href="http://www.tapptics.com">Tapptics</a> - thanks!
	Author: Tapptics, LLC & Baboons
        Author URI: http://www.tapptics.com/iphone-app-wordpress-theme/
	Version: 1.5 
*/ 

define('IPTEMP_PATH', plugin_dir_path(__FILE__) );
define('IPTEMP_LIBPATH', IPTEMP_PATH . '/lib/' );
define('IPTEMP_URL',  plugin_dir_url( __FILE__ ));
define('IPTEMP_TEMPLATEDIR', 'template');
define('IPTEMP_TEMPLATEPATH', IPTEMP_PATH . IPTEMP_TEMPLATEDIR );
define('IPTEMP_TEMPLATEURL',  IPTEMP_URL . IPTEMP_TEMPLATEDIR );
define('IPTEMP_POST_TYPE', 'iphone-app-page');


// Includes
require_once( IPTEMP_PATH . '/admin.php');
require_once( IPTEMP_PATH . '/cache.php');

// Actions
add_action( 'init', 'iptemp_init');
add_action( 'pre_get_posts', 'iptemp_pre_query' );
add_action( 'template_redirect', 'template_redirect');
function custom_page_menu($menu, $args) {
 // get supplied args
 $list_args = $args;

 // Overide some menu settings
 $list_args['echo'] = false;
 $list_args['title_li'] = '';
 $list_args['show_home'] = false;
 $list_args['exclude'] = 4; // excluding the homepage as I am manually adding it to the start below

 // get the current page object as we will need to refer to it when setting current items below
 global $wp_query;
 $current_page = $wp_query->get_queried_object();  
 // Show Home item at the start of the menu
 $menu ="";
 $menu .= '<li ' . $class . '><a href="' . home_url( '/' ) . '" title="' . esc_attr(__('Home')) . '">' . $args['link_before'] . __('Home') . $args['link_after'] . '</a></li>';



 $pt = IPTEMP_POST_TYPE;
 $obj_pt = get_post_type_object($pt);
 $list_args['post_type'] = $pt;
 //$menu .= '<li><a href="/' . $obj_pt->rewrite['slug'] . '/">' . $obj_pt->labels->name . '</a><ul>';

 // little bit of hacking here to force wp_list_pages to add current classes to
 // active pages in the generated navs. It's messy, but it means not having to
 // hack the WordPress core files
 $original_is_posts_page = $wp_query->is_posts_page;
 $wp_query->is_posts_page = true;
 $menu .= wp_list_pages($list_args);
 $wp_query->is_posts_page = $original_is_posts_page;



 // Now add the normal page collection which belongs in the nav
 $list_args['post_type'] = 'page';
 $menu .= wp_list_pages($list_args) ;

 // glue the menu together and send back
 if ( $menu )
 $menu = '<ul>' . $menu . '</ul>';

 $menu = '<div class="menu">' . $menu . "</div>\n";

 return $menu;
}
add_filter( 'wp_page_menu', 'custom_page_menu' ,10,2 );
function iptemp_init() {
 
    // Register Post Type
    register_post_type( IPTEMP_POST_TYPE,
        array(
            'labels' => array(
                'name' => __( 'iPhone App Page' ),
                'singular_name' => __( 'iPhone App Page' ),
                'add_new' => __( 'Add New Page' ),
                'add_new_item' => __( 'Add New Page' ),
                'edit' => __( 'Edit Page' ),
                'edit_item' => __( 'Edit Page' ),
                'new_item' => __( 'Add New Page' ),
                'view' => __( 'View Page' ),
                'view_item' => __( 'View Page' ),
                'search_items' => __( 'Search iPhone App Pages' ),
                'not_found' => __( 'No iPhone App Pages Found' ),
                'not_found_in_trash' => __( 'No iPhone App Pages found in Trash' ),
            ),
            'description' => __('iPhone App Page to be shown in Resources section.'),
            'public' => true,
            'show_ui' => true,
            'publicly_queryable' => true,
            'exclude_from_search' => false,
			'show_in_menu' => true, 
			'show_in_nav_menus' => true,
			'query_var' => IPTEMP_POST_TYPE,
            'menu_position' => 20,
			'capability_type' => 'post',
			'hierarchical'=> true,
			'has_archive' => true,
            'menu_icon' => IPTEMP_URL . '/images/post-type-icon.png',
            'supports' => array( 'title', 'thumbnail','page-attributes' ),
            'rewrite' => array( 'slug' => 'app', 'with_front' => false ),
            'can_export' => true
        )
    );                                  
} 


function iptemp_pre_query( $query ) {    
    // get id
    $iphone_app_homepage_id = get_option('iphone_app_homepage_id');
	if( ( isset( $_GET['iphone-app-page'] ) && $_GET['iphone-app-page'] != '' ) || ( get_post_type() == IPTEMP_POST_TYPE && isset( $_GET[ 'p' ] ) ) )
	{
		$query->set('post_type', IPTEMP_POST_TYPE);
	}
	
    // Check if page is home
     else if ( $query->is_home && $iphone_app_homepage_id != '' ) {
        if($query->query_vars['post_type'] != 'attachment') {
		$url = "?post_type=".IPTEMP_POST_TYPE."&p=".$iphone_app_homepage_id;
		header( "Location: $url" );
            $query->set('post_type', IPTEMP_POST_TYPE);            
        }
    }  
	//print_r($query);
	
}

function iptemp_is_domain()
{
    global $iptemp_is_domain;  
    
    if( !isset($iptemp_is_domain) ) return iptemp_get_domain();
    else return $iptemp_is_domain;
    
}

function iptemp_get_domain()
{
    global $iptemp_is_domain;
    
    // Run query
    $query = new WP_Query( 'post_type=' . IPTEMP_POST_TYPE. '&meta_key=iptemp-domain' );

    // Get domain
    $domain = $_SERVER['HTTP_HOST']; 
    
    foreach($query->posts as $post) {
        if( strstr($domain, get_post_meta( $post->ID, 'iptemp-domain', true ) ) ) {            
             $iptemp_is_domain = $post->ID;
             return true;
        }
    }
    
    $iptemp_is_domain=false;
    return false;
    
}

function template_redirect()
{
    global $wp_query, $posts, $post;
       
    // Get post type
    $custom_post_types = array(IPTEMP_POST_TYPE);
    
    // Test if we are searching for our post type
    if ( in_array($wp_query->query_vars["post_type"], $custom_post_types) || iptemp_is_domain() ) {
                
        // If domain is mapped run query
        if( iptemp_is_domain() ) {                   
            $domain = $_SERVER['HTTP_HOST'];            

            // Fetch correct post
            $args = array( 'p' => iptemp_is_domain(), 'post_type' => IPTEMP_POST_TYPE, 'numberposts' => 1, 'posts_per_page' =>1 );        
            $posts = query_posts( $args );

            // Set the new post
            while ( have_posts() ) : the_post(); endwhile;
            //return str_replace( get_site_url(), "http://$domain", IPTEMP_TEMPLATEURL);              
        }

        // Create cached page               
        if( get_option('iptemp_cache') )
            iptempCache::init()->save();
            
        
	// Apply filters
        // stylesheet_directory 
        add_filter('stylesheet_directory_uri','wpi_stylesheet_dir_uri',10,2);
                
        // Load template
        load_template(IPTEMP_TEMPLATEPATH . '/index.php');        
        exit;
    }
 }

function wpi_stylesheet_dir_uri($stylesheet_dir_uri, $theme_name){	
    return IPTEMP_TEMPLATEURL;
}

function get_orderd_meta($post_id, $key)
{
    global $wpdb;
    
    $meta = get_post_meta( $post_id, $key );
    if ( empty( $meta ) )
            return array();

    $meta = implode( ',' , $meta );

    // Re-arrange images with 'menu_order', thanks Onur
    $meta = $wpdb->get_col( "
            SELECT ID FROM {$wpdb->posts}
            WHERE post_type = 'attachment'
            AND ID in ({$meta})
            ORDER BY menu_order ASC
    " );

    return (array) $meta;
}
