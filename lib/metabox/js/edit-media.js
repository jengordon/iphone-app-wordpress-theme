function closeMediaEdit(e) {

        $(e).parents( '#rwm-edit-container' ).slideUp('fast', function() { $(this).remove() } );
	return true;
}

function saveMediaEdit(e) {
    
        var     e		= $(e),
                post_id 	= e.parents( '#rwm-edit-container' ).find('#field_post_id').val(),
                post_title 	= e.parents( '#rwm-edit-container' ).find('#field_post_title').val(),
                post_excerpt 	= e.parents( '#rwm-edit-container' ).find('#field_post_excerpt').val()
                post_content 	= e.parents( '#rwm-edit-container' ).find('#field_post_content').val(),
                data		= {
                        action:		'rwmb_edit_media_save',
                        post_id:	post_id,
                        post_title:	post_title,
                        post_excerpt:	post_excerpt,
                        post_content:   post_content
                };
        
        $.post( ajaxurl, data, function( r ) 
        {
                var res = wpAjax.parseAjaxResponse( r, 'ajax-response' );
                if ( res.errors )
                {
                        alert( res.responses[0].errors[0].message );
                } else {
                     res		= res.responses[0];                     
                     e.parents( '#rwm-edit-container' ).slideUp('fast', function() { $(this).remove() } );
                }
                

        }, 'xml' );
        
        return true;
}

jQuery( document ).ready( function($) {

    $('a.rwmb-edit-media').click( function(event) {
        // prevent default 
        event.preventDefault();
        
        var $this		= $(this),
                field_id	= $this.parents( '.rwmb-field' ).find( '.field-id' ).val(),
                data		= {
                        action:		'rwmb_edit_media',
                        _wpnonce:	$( '#nonce-edit-media_' + field_id ).val(),
                        post_id:	$( '#post_ID' ).val(),
                        field_id:	field_id,
                        attachment_id:	$this.attr('rel')
                };
        
        $.post( ajaxurl, data, function( r ) 
        {
                var res = wpAjax.parseAjaxResponse( r, 'ajax-response' );
                if ( res.errors )
                {
                        alert( res.responses[0].errors[0].message );
                } else {
                     res		= res.responses[0];                     
	             $('#rwm-edit-container').slideUp('fast', function() { $(this).remove() } );
		     $('<div id="rwm-edit-container">' + res.data + '</div>').insertAfter( $this.parents( '.rwmb-field' ).find('.rwmb-images') ).hide().slideDown('fast');
                }
                

        }, 'xml' );
        
        return true;
    });
    
    
    
} );
