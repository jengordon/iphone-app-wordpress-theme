<?php
// Prevent loading this file directly - Busted!
if( ! class_exists('WP') ) 
{
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit;
}

if ( ! class_exists( 'RWMB_Cols_Field' ) ) 
{
	class RWMB_Cols_Field 
	{
		/**
		 * Get field HTML
		 *
		 * @param string $html
		 * @param mixed  $meta
		 * @param array  $field
		 *
		 * @return string
		 */
		static function html( $html, $meta, $field ) 
		{              
                        $i=0;
                        
                        
                        
                        foreach($field['cols'] as $col => $col_title) {
                            $i++;
                            
                            $type = isset( $field['cols_settings'][$col]['type'] ) ? $field['cols_settings'][$col]['type'] : false;
                            
                            $value = isset( $meta[$col] ) ? $meta[$col] : false;
                            $std		 = isset( $field['disabled'] ) ? $field['disabled'] : false;
                            $disabled            = disabled( $std, true, false );
                            $colid               = $field['id'] . "-$col";
                            $name		 = "name='{$colid}[]'";
                            $id			 = " id='$colid'";
                            $val		 = " value='{$value}'";
                            $size		 = isset( $field['size'] ) ? $field['size'] : '40';                            
                            //$html               .= "<h4>$col_title</h4>";
                            $html                .= "<div class='rwmb-field'>";
                            $html                .= "<div class='rwmb-label'>$col_title</div>";
                            $html                .= "<div class='rwmb-input' style='width: 70%;'>";
                            if($type == 'textarea')
                                $html		 .= "<textarea class='rwmb-text'{$name}{$id}{$disabled} rows='5' cols='60'>$value</textarea>";
                            else
                                $html		 .= "<input type='text' class='rwmb-text'{$name}{$id}{$val}{$disabled} size='{$size}' />";
                                
                            $html                .= "</div>";
                            $html                .= "</div>";                            
                            
                            
                        }
                        
                        $html .= "<hr style='margin: 20px 0px; border: 0; height: 1px; color: #e1e1e1; background-color: #e1e1e1;' />";
                                
                        
			return $html;
		}
                
		static function meta( $meta, $post_id, $saved, $field )
		{
                        $meta = array();
                        
                        foreach($field['cols'] as $col => $col_title) {                                                        
                            $meta['cols'][$col] = get_post_meta( $post_id, $field['id'] . "-$col", ! $field['multiple'] );                       
                        }
                                                                        
			return $meta;
		}
                
		static function save( $new, $old, $post_id, $field )
		{
			foreach($field['cols'] as $col => $col_title) {
				$name = $field['id'] . "-$col";

				$old = get_post_meta( $post_id, $name, ! $field['multiple'] );
				self::savemeta( $_REQUEST[$name], $old, $post_id, $name, $field );
			}
		}
                
        static function savemeta( $new, $old, $post_id, $name, $field ) {
			
			delete_post_meta( $post_id, $name );
			if ( '' === $new || array() === $new )
				return;

			if ( $field['multiple'] ) {
				foreach ( $new as $add_new ) {
					add_post_meta( $post_id, $name, $add_new, false );
				}
			} else {
				if( ! empty( $new[0] ) ) {
					update_post_meta( $post_id, $name, $new );
				}
			}
		}
	}
}