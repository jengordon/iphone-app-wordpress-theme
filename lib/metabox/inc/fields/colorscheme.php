<?php
// Prevent loading this file directly - Busted!
if( ! class_exists('WP') ) 
{
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit;
}

if ( ! class_exists( 'RWMB_Colorscheme_Field' ) ) 
{
	class RWMB_Colorscheme_Field 
	{
		/**
		 * Enqueue scripts and styles
		 * 
		 * @return	void
		 */
		static function admin_print_styles( ) 
		{
			wp_enqueue_style( 'rwmb-select', RWMB_CSS_URL.'select.css', RWMB_VER );
		}

		/**
		 * Get field HTML
		 *
		 * @param string $html
		 * @param mixed  $meta
		 * @param array  $field
		 *
		 * @return string
		 */
		static function html( $html, $meta, $field ) 
		{

                        $html = '<input type="hidden" name="iptemp-color-scheme-name" id="iptemp_color_scheme" value="' . $meta . '" />';
                        $html .= '<div class="color_scheme_cont">';
                        
                        foreach($field['schemes'] as $scheme) {
                            $class = $scheme == $meta ? 'current_scheme' : '';
                            $src = IPTEMP_URL . "/images/iphone-theme-$scheme.png";                            
                            $html .= '<img src="' . $src . '" alt="' . $scheme . '" class="' . $class . '" />';
                        }
                        $html .= '</div>';
                        
                        ob_start();
                        ?>
                        
                        <script type="text/javascript">                                                      
                        $('.color_scheme_cont img').click(function() {
                            $('.color_scheme_cont img').removeClass('current_scheme');
                            $(this).addClass('current_scheme');                            
                            $('#iptemp_color_scheme').val($(this).attr('alt'));
                        });
                        </script>
                        
                        <?php
                        $html .= ob_get_contents();
                        ob_end_clean();

			return $html;
		}
	}
}