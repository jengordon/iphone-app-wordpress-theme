<?php
	global $post;
	$is_landscape = get_post_meta( $post->ID, 'iptemp-applandscape', true );
	$orientation = $is_landscape ? 'landscape' : 'portrait';

	$current_scheme = get_post_meta( $post->ID, 'iptemp-color-scheme-name', true );

	function ip_get_download_button() {
		global $post;
		$current_scheme = get_post_meta( $post->ID, 'iptemp-color-scheme-name', true );
		$has_url = get_post_meta($post->ID, 'iptemp-app-itunes-url', true );
		ob_start(); ?>
		<div class="download_from_app_store_cont <?php echo $current_scheme ?>">
			<div class="app_store_price_cont"><?php echo get_post_meta($post->ID,"iptemp-app-price",true); ?></div>
			<?php if( $has_url ) { ?>
			  <a href="<?php echo get_post_meta( $post->ID, 'iptemp-app-itunes-url', true ); ?>">
			<?php } ?>
			<?php if( $has_url ) echo '</a>'; ?>
		</div><!-- .download_from_app_store_cont --><?php
		return ob_get_clean();
	}

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml">
<head>
  <title><?php the_title(); ?></title>          
  <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/iphone-template.css" type="text/css" media="screen" title="no title" charset="utf-8"/>
  <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/iphone-custom.css" type="text/css" media="screen" title="no title" charset="utf-8"/>
  <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.min.js"></script>
  <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/js/themes/default/jquery.lightbox.css" />
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.lightbox.min.js"></script>      
  <script type="text/javascript">
    jQuery(document).ready(function($){
      $('.lightbox').lightbox();
      
      <?php if($current_scheme == 'white-green') { ?>
      $(".subscribe_button")
          .mouseover(function() { 
              $(this).attr("src","<?php bloginfo('stylesheet_directory'); ?>/images/subscribe-button-hover.png");
          })
          .mouseout(function() {
              $(this).attr("src","<?php bloginfo('stylesheet_directory'); ?>/images/subscribe-button.png");
          });      

      $(".send_message_button")
          .mouseover(function() { 
              $(this).attr("src","<?php bloginfo('stylesheet_directory'); ?>/images/send-message-button-hover.png");
          })
          .mouseout(function() {
              $(this).attr("src","<?php bloginfo('stylesheet_directory'); ?>/images/send-message-button.png");
          });      
      <?php } elseif($current_scheme == 'white-orange') { ?>
      $(".subscribe_button")
          .mouseover(function() { 
              $(this).attr("src","<?php bloginfo('stylesheet_directory'); ?>/images/subscribe-button-white-orange-hover.png");
          })
          .mouseout(function() {
              $(this).attr("src","<?php bloginfo('stylesheet_directory'); ?>/images/subscribe-button-white-orange.png");
          });      

      $(".send_message_button")
          .mouseover(function() { 
              $(this).attr("src","<?php bloginfo('stylesheet_directory'); ?>/images/send-message-button-white-orange-hover.png");
          })
          .mouseout(function() {
              $(this).attr("src","<?php bloginfo('stylesheet_directory'); ?>/images/send-message-button-white-orange.png");
          });    
      <?php } elseif($current_scheme == 'white-red') { ?>
      $(".subscribe_button")
          .mouseover(function() { 
              $(this).attr("src","<?php bloginfo('stylesheet_directory'); ?>/images/subscribe-button-white-red-hover.png");
          })
          .mouseout(function() {
              $(this).attr("src","<?php bloginfo('stylesheet_directory'); ?>/images/subscribe-button-white-red.png");
          });      

      $(".send_message_button")
          .mouseover(function() { 
              $(this).attr("src","<?php bloginfo('stylesheet_directory'); ?>/images/send-message-button-white-red-hover.png");
          })
          .mouseout(function() {
              $(this).attr("src","<?php bloginfo('stylesheet_directory'); ?>/images/send-message-button-white-red.png");
          });      
      <?php } elseif($current_scheme == 'black-green') { ?>
      $(".subscribe_button")
          .mouseover(function() { 
              $(this).attr("src","<?php bloginfo('stylesheet_directory'); ?>/images/subscribe-button-hover.png");
          })
          .mouseout(function() {
              $(this).attr("src","<?php bloginfo('stylesheet_directory'); ?>/images/subscribe-button.png");
          });      

      $(".send_message_button")
          .mouseover(function() { 
              $(this).attr("src","<?php bloginfo('stylesheet_directory'); ?>/images/send-message-button-hover.png");
          })
          .mouseout(function() {
              $(this).attr("src","<?php bloginfo('stylesheet_directory'); ?>/images/send-message-button.png");
          });    
      <?php } elseif($current_scheme == 'black-orange') { ?>
      $(".subscribe_button")
          .mouseover(function() { 
              $(this).attr("src","<?php bloginfo('stylesheet_directory'); ?>/images/subscribe-button-white-orange-hover.png");
          })
          .mouseout(function() {
              $(this).attr("src","<?php bloginfo('stylesheet_directory'); ?>/images/subscribe-button-white-orange.png");
          });      

      $(".send_message_button")
          .mouseover(function() { 
              $(this).attr("src","<?php bloginfo('stylesheet_directory'); ?>/images/send-message-button-white-orange-hover.png");
          })
          .mouseout(function() {
              $(this).attr("src","<?php bloginfo('stylesheet_directory'); ?>/images/send-message-button-white-orange.png");
          });      
      <?php } else { ?>
      $(".subscribe_button")
          .mouseover(function() { 
              $(this).attr("src","<?php bloginfo('stylesheet_directory'); ?>/images/subscribe-button-white-red-hover.png");
          })
          .mouseout(function() {
              $(this).attr("src","<?php bloginfo('stylesheet_directory'); ?>/images/subscribe-button-white-red.png");
          });      

      $(".send_message_button")
          .mouseover(function() { 
              $(this).attr("src","<?php bloginfo('stylesheet_directory'); ?>/images/send-message-button-white-red-hover.png");
          })
          .mouseout(function() {
              $(this).attr("src","<?php bloginfo('stylesheet_directory'); ?>/images/send-message-button-white-red.png");
          });            
      <?php } ?>

<?php

$i=0;
$screenshots = get_orderd_meta( $post->ID, 'iptemp-screenshots' );
$videos = array();    

foreach($screenshots as $image_id) {    
    $image = wp_get_attachment_image_src($image_id, 'full');
    $meta = get_post($image_id);  
    if( !empty($meta->post_content) ) $videos[$i] = $meta->post_content;
    ?>
      $(".iphone_screen<?php echo $i; ?>_click").bind('click mouseover', function() {            
            $("#iphone_image").attr("src","<?php echo $image[0]; ?>");
           // $("#iptemp-headline,#iptemp-body").hide();
            //$("#iptemp-headline-hover,#iptemp-body-hover").show();                        
           // $("#iptemp-headline-hover").html('<?php echo esc_html($meta->post_title)?>');
            //$("#iptemp-body-hover").html('<?php echo esc_html($meta->post_excerpt)?>');
            //$(".featured_bubble").html('<h2><?php echo esc_html($meta->post_title)?></h2><?php echo esc_html($meta->post_excerpt) ?>');
            //$(".featured_bubble").css("display","block");
            //$(".featured_bubble_close_box").css("display","block");            
            
      }); 
      
      $(".iphone_screen<?php echo $i; ?>_click").bind('mouseleave', function() {                        
            //$("#iphone_image").attr("src", $(".iphone_image").attr("src") );
           // $("#iptemp-headline,#iptemp-body").show();
           // $("#iptemp-headline-hover,#iptemp-body-hover").hide();                        
      });
<?php $i++; } ?>

// Open first screen
//$(".iphone_screen0_click").click();

//closing the iphone popup text
$(".featured_bubble_close_box").click(function() {
    $(".featured_bubble").css("display","none");
    $(".featured_bubble_close_box").css("display","none");
});

    });
    
  </script>  
</head>
<body class="<?php echo str_replace('-', '_', $current_scheme)?> <?php echo substr($current_scheme, 0, strpos($current_scheme,'-'))?>_theme <?php echo $orientation; ?>">
<?php  if (have_posts()) : while (have_posts()) : the_post(); ?>

<div style="display: none;">
<!--    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/iphone-screen1.jpg" />
    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/iphone-screen2.png" />
    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/iphone-screen3.jpg" />
    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/iphone-screen4.jpg" />-->
<?php if($current_scheme == 'white-orange') { ?>
    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/subscribe-button-white-orange-hover.png" />
    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/send-message-button-white-orange-hover.png" />
<?php } elseif($current_scheme == 'white-red') { ?>    
    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/subscribe-button-white-red-hover.png" />
    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/send-message-button-white-red-hover.png" />
<?php } elseif($current_scheme == 'black-green') { ?>        
    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/subscribe-button-hover.png" />
    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/send-message-button-hover.png" />
<?php } elseif($current_scheme == 'black-orange') { ?>            
    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/subscribe-button-white-orange-hover.png" />
    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/send-message-button-white-orange-hover.png" />
<?php } elseif($current_scheme == 'black-red') { ?>                
    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/subscribe-button-white-red-hover.png" />
    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/send-message-button-white-red-hover.png" />
<?php } else { ?>
    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/subscribe-button-hover.png" />
    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/send-message-button-hover.png" />
<?php } ?>    


<?php foreach($videos as $k => $video): ?>
    <div id="video_inline_cont<?php echo $k; ?>">
        <?php echo $video; ?>
    </div>
<?php endforeach ?>    
    
<!--//inline html for video-->
<?php /*$vid_counter = 0;
for($x=0;$x<count($screenshot_img_arr);$x++) {     

    if($screenshot_img_arr[$x] == '--not applicable--') { ?>
        
        <div id="video_inline_cont<?php echo $vid_counter; ?>">
            <?php echo $screenshot_desc_arr[$x]; ?>
        </div>
        
    <?php $vid_counter++; }

} */ ?>



</div>

<div id="outside_container">

    <div id="main_container">

        <div id="header">
            
            <div class="left">

                <?php if( get_post_meta( $post->ID, 'iptemp-top-app-icon', true )==''): ?>
                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/logo.png" class="logo" />              
                <?php else: ?>
                    <?php $image = wp_get_attachment_image_src( get_post_meta( $post->ID, 'iptemp-top-app-icon', true ) ); ?>
                    <img src="<?php echo $image[0] ?>" class="logo" alt="logo" />
                <?php endif ?>
                <p class="title"><?php echo get_post_meta($post->ID,"iptemp-appname",true); ?></p>
                <p class="subtitle"><?php echo get_post_meta($post->ID,"iptemp-apptagline",true); ?></p>
                <div class="clear"></div>

            </div>
            
            <div class="right">

                <ul>
                  <?php if( get_post_meta( $post->ID, 'iptemp-twitter-id', true ) != '' || get_post_meta( $post->ID, 'iptemp-facebook-url', true ) != '' || get_post_meta( $post->ID, 'iptemp-linkedin-url', true ) != ''): ?>
                    <li class="share_text">Share this page:</li>
                  <?php endif ?>
                  
                  
                  <?php if($current_scheme == 'black-red' || $current_scheme == 'black-green' || $current_scheme =='black-orange'): ?>                  

                    <?php if(get_post_meta( $post->ID, 'iptemp-linkedin-url', true ) != ''): ?>
                      <li><a href="<?php echo get_post_meta( $post->ID, 'iptemp-linkedin-url', true ); ?>"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/linkedin-icon-black.png" /></a></li>
                    <?php endif ?>
                      
                    <?php if(get_post_meta( $post->ID, 'iptemp-twitter-id', true ) != ''): ?>
                      <li><a href="http://twitter.com/<?php echo get_post_meta( $post->ID, 'iptemp-twitter-id', true ); ?>"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/twitter-icon-black.png" /></a></li>
                    <?php endif ?>
                    
                    <?php if(get_post_meta( $post->ID, 'iptemp-facebook-url', true ) != ''): ?>
                      <li><a href="<?php echo get_post_meta( $post->ID, 'iptemp-facebook-url', true ); ?>"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/facebook-icon-black.png" /></a></li>
                    <?php endif ?>                  
                  
                  <?php else: ?>
                      
                    <?php if(get_post_meta( $post->ID, 'iptemp-linkedin-url', true ) != ''): ?>
                      <li><a href="<?php echo get_post_meta( $post->ID, 'iptemp-linkedin-url', true ); ?>"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/linkedin-icon.png" /></a></li>
                    <?php endif ?>  
                      
                    <?php if(get_post_meta( $post->ID, 'iptemp-twitter-id', true ) != ''): ?>
                      <li><a href="http://twitter.com/<?php echo get_post_meta( $post->ID, 'iptemp-twitter-id', true ); ?>"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/twitter-icon.png" /></a></li>
                    <?php endif; ?>
                    
                    <?php if(get_post_meta( $post->ID, 'iptemp-facebook-url', true ) != ''): ?>
                      <li><a href="<?php echo get_post_meta( $post->ID, 'iptemp-facebook-url', true ); ?>"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/facebook-icon.png" /></a></li>
                    <?php endif ?>                

                  <?php endif ?>
                </ul>

                <div class="clear"></div>

            </div>

            <div class="clear"></div>
            
        </div><!-- .header -->

        <div id="content">

			<div class="left-column left">

				<div class="iphone_desc">

					<div class="iphone_desc_body">

						<?php if($current_scheme == 'black-red') { ?>
						<span class="font_27 color_aqua">
						<?php } elseif($current_scheme == 'black-green') { ?>
						<span class="font_27 color_yellow">
						<?php } elseif($current_scheme == 'black-orange') { ?>
						<span class="font_27 color_green">
						<?php } else { ?>
						<span class="font_27 color_gray">
						<?php } ?>

							<div id="iptemp-headline">
								<?php echo get_post_meta($post->ID,"iptemp-headline",true); ?>
							</div>

							<div id="iptemp-headline-hover"></div>

						</span>

						<div id="iptemp-body">
							<?php echo get_post_meta($post->ID,"iptemp-body-text",true); ?>
						</div>

						<div id="iptemp-body-hover"></div>

						<?php if( ! $is_landscape ) echo ip_get_download_button(); ?>

					</div><!-- .iphone_desc_body -->

				</div><!-- .iphone_desc left -->

				<div class="bottom_content">

					<?php if($current_scheme == 'black-red' || $current_scheme == 'black-green' || $current_scheme =='black-orange') { ?>
					  <ul class="thumbnail_list thumbnail_list_black">
					<?php } else { ?>
					  <ul class="thumbnail_list">
					<?php } ?>

					 <?php $i=0; foreach($screenshots as $image_id): ?>
						<?php $image = wp_get_attachment_image_src($image_id); ?>

							<?php if(isset($videos[$i])): ?>
								<li>
									<a href="?lightbox[width]=570&lightbox[height]=315#video_inline_cont<?php echo $k ?>" class="lightbox">
										<img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/thumbnail-current.png" />
									</a>
								</li>
							<?php else: ?>
								<li>
									<img src="<?php echo $image[0] ?>" class="iphone_screen<?php echo $i ?>_click" />
								</li>
							<?php endif; ?>

					 <?php $i++; endforeach ?>
					</ul>

					<div class="clear"></div>

					<?php if( $is_landscape ) echo ip_get_download_button(); ?>

					<?php // Key Features
						$features = get_post_meta( $post->ID, 'iptemp-key-features', true );
						if( count( $features ) == 1 && $features[0] == '' ) $features = null;

						if( is_array($features) ): ?>
							<h3>Key Features</h3>
							<ul class="key_features">
								<?php foreach( $features as $feature): ?>
									<li><h4><?php echo $feature; ?></h4></li>
								<?php endforeach ?>
							</ul>
							<div class="clear"></div><?php
						endif; ?>

					<?php
						$main_quote_arr = get_post_meta( $post->ID, 'iptemp-user-reviews-main-quote' );
						$body_quote_arr = get_post_meta( $post->ID, 'iptemp-user-reviews-body-quote' );
						$user_review_name_arr = get_post_meta( $post->ID, 'iptemp-user-reviews-name' );
						$user_review_url_arr = get_post_meta( $post->ID, 'iptemp-user-reviews-url-link' );
						$user_review_url_text_arr = get_post_meta( $post->ID, 'iptemp-user-reviews-url-text' );
						$user_counter = 0;
						?>

						<?php if( is_array($main_quote_arr) ): ?>
							<h3><?php echo get_post_meta( $post->ID, 'iptemp-user-reviews-title', true ); ?></h3>
							<?php for( $x = 0; $x < count( $main_quote_arr ); $x++ ): ?>
								<?php if( ! empty( $main_quote_arr[$x] ) ): ?>

									<div class="user_reviews_box <?php echo $user_counter % 2 ? 'right' : 'left'; ?>">

										<h4><?php echo $main_quote_arr[$x]; ?></h4>
										<?php if( ! empty( $body_quote_arr[$x] ) ) : ?>
											<p><i><?php echo $body_quote_arr[$x]; ?></i></p>
										<?php endif; ?>
										<p align="right" class="user_name"><?php
											if( ! empty( $user_review_name_arr[$x] ) ) {
												echo $user_review_name_arr[$x] . ', ';
											}
											if( ! empty( $user_review_url_arr[$x] ) && ! empty( $user_review_url_text_arr[$x] ) ){
												echo '<a href="' . $user_review_url_arr[$x] . '">' . $user_review_url_text_arr[$x] . '</a>';
											} ?>
										</p>

									</div><!-- .user_reviews_box -->
								<?php $user_counter++; if(!$user_counter%2) { echo '<div class="clear"></div>'; $user_counter = 0; }   ?>
								<?php endif; ?>
							<?php endfor; ?>
						<?php endif; ?>

					<div class="clear"></div>

					<?php if( get_post_meta($post->ID,"iptemp-hide-subscribe",true) ): ?>

					<p><span class="font_24"><?php echo get_post_meta($post->ID,"iptemp-email-headline",true); ?></span><br />
					<?php echo get_post_meta($post->ID,"iptemp-email-body-text",true); ?></p>
					<a name="subform"></a>

					<?php if( ! empty( $_POST['subscribe_email_address'] ) ) { ?>

						<?php
						  $myFile = ABSPATH . "/.email_subscribe_list.txt";
						  $fh = fopen("$myFile", 'a') or die("can't open file");
						  $stringData = $_REQUEST['subscribe_email_address'] . ",";
						  fwrite($fh, $stringData);
						  fclose($fh);
						?>

						<div class="success_msg">
							<h4 align="center">Successfully sent!</h4>
						</div><!--//success_msg-->

					<?php } else { ?>

						<form id="iphone_subscribe_form" method="post" action="#subform">
						<input type="text" value="enter email address" name="subscribe_email_address" id="subscribe_email_field" onclick="if(this.value == 'enter email address') this.value='';" onblur="if(this.value == '') this.value='enter email address';" />
							<?php if($current_scheme == 'white-orange') { ?>
							  <input type="image" SRC="<?php bloginfo('stylesheet_directory'); ?>/images/subscribe-button-white-orange.png" class="subscribe_button" BORDER="0" ALT="Submit Form" />
							<?php } elseif($current_scheme == 'white-red') { ?>
							  <input type="image" SRC="<?php bloginfo('stylesheet_directory'); ?>/images/subscribe-button-white-red.png" class="subscribe_button" BORDER="0" ALT="Submit Form" />
							<?php } elseif($current_scheme == 'black-green') { ?>
							  <input type="image" SRC="<?php bloginfo('stylesheet_directory'); ?>/images/subscribe-button.png" class="subscribe_button" BORDER="0" ALT="Submit Form" />
							<?php } elseif($current_scheme == 'black-orange') { ?>
							  <input type="image" SRC="<?php bloginfo('stylesheet_directory'); ?>/images/subscribe-button-white-orange.png" class="subscribe_button" BORDER="0" ALT="Submit Form" />
							<?php } elseif($current_scheme == 'black-red') { ?>
							  <input type="image" SRC="<?php bloginfo('stylesheet_directory'); ?>/images/subscribe-button-white-red.png" class="subscribe_button" BORDER="0" ALT="Submit Form" />
							<?php } else { ?>
							  <input type="image" SRC="<?php bloginfo('stylesheet_directory'); ?>/images/subscribe-button.png" class="subscribe_button" BORDER="0" ALT="Submit Form" />
							<?php } ?>
						<p id="subscribe_error_msg" class="err_msg"></p>
						</form>

					<?php } ?>

					<script type="text/javascript">
					$('#iphone_subscribe_form').submit(function() {
						var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
						if(!regex.test($('#subscribe_email_field').val())) {
							$('#subscribe_error_msg').html('enter a valid email address');
							return false;
						} else {
							return true;
						}
					});
				  </script>
				  <?php endif; ?>

				<div class="clear"></div>

					<?php if( get_post_meta( $post->ID, 'iptemp-contact-form-email', true ) ): ?>
						<h3>Contact Us</h3>

						<a name="cform"></a>
						<?php
							if($_POST['full_name'] != '' && get_post_meta( $post->ID, 'iptemp-contact-form-email', true ) != '') {

							$to = get_post_meta( $post->ID, 'iptemp-contact-form-email', true );
							$subject = 'iPhone Contact Form';

							$message = '<p>Full Name: ' . $_REQUEST['full_name'] . '</p>';
							$message .= '<p>Email Address: ' . $_REQUEST['email_address'] . '</p>';
							$message .= '<p>Message: ' . $_REQUEST['your_message'] . '</p>';

							$headers  = 'MIME-Version: 1.0' . "\r\n";
							$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
							$headers .= 'To: ' . get_post_meta( $post->ID, 'iptemp-contact-form-email', true ) . ' <' . get_post_meta( $post->ID, 'iptemp-contact-form-email', true ) . '>' . "\r\n";
							//$headers .= 'From: Me <me@example.com>' . "\r\n";

							if(mail($to, $subject, $message, $headers)) {
								//echo '<p><b>Thank you for contacting us.</b></p>'; ?>


					<div class="success_msg">
						<h4 align="center">Your message has been sent.</h4>
	<!--                    <p>This is the message that appears after the person has submitted the contact form. It is usually a &quot;Thanks - we'll be in touch shortly!&quot;</p>-->
					</div><!--//success_msg-->

								<?php }

					} else {

					?>


					<p>Do you have a feature request or support question? Get in touch with us below!</p>

					<form method="post" id="iphone_contact_form" action="#cform">
					<div class="contact_form_cont">
						<input type="text" value="full name" id="full_name_field" name="full_name" onclick="if(this.value == 'full name') this.value='';" onblur="if(this.value == '') this.value='full name';" /> <span id="full_name_message" class="err_msg"></span><br />
						<input type="text" value="email address" id="email_address_field" name="email_address" onclick="if(this.value == 'email address') this.value='';" onblur="if(this.value == '') this.value='email address';" /> <span id="email_address_message" class="err_msg"></span><br />
						<textarea name="your_message" onclick="if(this.value == 'your message goes here') this.value='';" onblur="if(this.value == '') this.value='your message goes here';">your message goes here</textarea>
						<div align="right">
							<?php if($current_scheme == 'white-orange') { ?>
							  <INPUT TYPE="image" SRC="<?php bloginfo('stylesheet_directory'); ?>/images/send-message-button-white-orange.png" class="send_message_button" BORDER="0" ALT="Submit Form">
							<?php } elseif($current_scheme == 'white-red') { ?>
							  <INPUT TYPE="image" SRC="<?php bloginfo('stylesheet_directory'); ?>/images/send-message-button-white-red.png" class="send_message_button" BORDER="0" ALT="Submit Form">
							<?php } elseif($current_scheme == 'black-green') { ?>
							  <INPUT TYPE="image" SRC="<?php bloginfo('stylesheet_directory'); ?>/images/send-message-button.png" class="send_message_button" BORDER="0" ALT="Submit Form">
							<?php } elseif($current_scheme == 'black-orange') { ?>
							  <INPUT TYPE="image" SRC="<?php bloginfo('stylesheet_directory'); ?>/images/send-message-button-white-orange.png" class="send_message_button" BORDER="0" ALT="Submit Form">
							<?php } elseif($current_scheme == 'black-red') { ?>
							  <INPUT TYPE="image" SRC="<?php bloginfo('stylesheet_directory'); ?>/images/send-message-button-white-red.png" class="send_message_button" BORDER="0" ALT="Submit Form">
							<?php } else { ?>
							  <INPUT TYPE="image" SRC="<?php bloginfo('stylesheet_directory'); ?>/images/send-message-button.png" class="send_message_button" BORDER="0" ALT="Submit Form">
							<?php } ?>
						</div>
					</div><!--//contact_form_cont-->
					</form>

					<?php } ?>

					<script type="text/javascript">
					$('#iphone_contact_form').submit(function() {

						var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

						if($('#full_name_field').val() == '' || $('#full_name_field').val() == 'full name') {
							$('#full_name_message').html('&lt; required field');
							return false;
						} else if(!regex.test($('#email_address_field').val())) {
							$('#email_address_message').html('&lt; enter a valid email address');
							return false;
						} else {
							return true;
						}
					});
				  </script>
				  <?php endif; ?>
				</div><!-- .bottom_content -->

			</div><!-- .left -->

			<div class="right-column right">

				<div class="iphone_cont">
					<div class="featured_bubble">
						<?php echo $screenshot_desc_arr[0]; ?>
					</div><!--//featured_bubble-->
					<div class="iphone_image">
						<img src="<?php $imagea = wp_get_attachment_image_src($screenshots[0], 'full'); echo ! empty($imagea[0]) ? $imagea[0] : false; ?>" alt=" " id="iphone_image" />
					</div><!--//iphone_image-->
				</div><!--//iphone_cont-->

				<div class="bottom_sidebar">

					<h3><?php echo get_post_meta($post->ID,"iptemp-about-us-title",true); ?></h3>

					<p><?php echo get_post_meta($post->ID,"iptemp-company-bio",true); ?></p>

					<ul class="our_members">

					<?php
						$members = get_orderd_meta( $post->ID, "iptemp-team-member" );

						foreach($members as $member_id) :
							$image = wp_get_attachment_image_src($member_id, 'full');
							$meta = get_post($member_id);
							$member_name = $meta->post_title;
							$member_title = $meta->post_excerpt;
							$member_bio = $meta->post_content; ?>
							<li>
								<img src="<?php echo $image[0]; ?>" width="77" height="84" />
								<h3><?php echo $member_name ?> <i><?php echo $member_title ?> </i></h3>
								<p><?php echo $member_bio ?></p>
							</li><?php
						endforeach; ?>
					</ul>

					<?php
						$apps = get_orderd_meta( $post->ID, "iptemp-other-apps" );
						if( count($apps) > 0 ): ?>
							<h3 class="font_18">Our Other Apps</h3>
							<ul class="other_apps">
								<?php foreach( $apps as $app_id ) :
									$image = wp_get_attachment_image_src($app_id, 'full');
									$meta = get_post($app_id);
									$app_name = $meta->post_title;
									$app_url = $meta->post_content; ?>
									<li>
										<img src="<?php echo $image[0]; ?>" width="67" height="67" />
										<p>
											<a href="<?php echo $app_url; ?>"><?php echo $app_name; ?></a>
										</p>
									</li>
								<?php endforeach; ?>
							</ul><?php
						endif; ?>

					<div class="clear"></div>
				</div><!-- .bottom_sidebar -->

			</div><!-- .right -->

            <div class="clear"></div>
            
        </div><!--//content-->

    </div><!--//main_container-->
    
    <div id="footer_full">
    
        <div class="footer_top"></div>
        
        <?php if($current_scheme == 'white-orange' || $current_scheme == 'black-orange') { ?> 
          <div id="footer" class="orange_footer">
        <?php } elseif($current_scheme == 'white-green' || $current_scheme == 'black-green') { ?>
          <div id="footer" class="green_footer">
        <?php } else { ?>
          <div id="footer" class="red_footer">
        <?php } ?>
        Free <a href="http://www.tapptics.com/iphone-app-wordpress-theme/">iPhone App Wordpress Theme</a> by Jen Gordon at <a href="http://www.tapptics.com/">www.tapptics.com</a>
        <div style="float: right">Development by Johan Johansson at <a href="http://www.baboons.se/">baboons.se</a> and  <a href="mailto:amitaymolko@gmail.com">Amitay Molko</a></div>
        </div><!--//footer-->
    
    </div><!--//footer_full-->
    
</div><!--//outside_container-->

<?php endwhile; else: ?>

    <h3>Sorry, no posts matched your criteria.</h3>

<?php endif; ?>            

</body>
</html>