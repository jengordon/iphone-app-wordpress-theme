<?php

class iptempCache {
    
    private static $instance;
        
    function __construct() {
        // Set paths
        $this->cache_base = WP_CONTENT_DIR . "/cache/iptemp/"; 
        $this->cache_path = $this->cache_base . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];        
        $this->cache_file = $this->cache_path . 'index.html';
                             
    }

    public function save() {
        // Time
        $this->cache_start = microtime(true);
        
        // Start cache        
        ob_start( array($this, 'iptemp_cache_ob_callback') );

        // Register shutdown callback
        register_shutdown_function('iptemp_cache_shutdown_callback');        
    }
    
    public function delete($post_id) {
        // Get post
        $post = get_post($post_id);
        
        // Test type
        if( $post->post_type != IPTEMP_POST_TYPE ) return;

        // Delete cache dir
        if( is_dir( $this->cache_base ) )
            $this->deleteAll( $this->cache_base );                
    }
    
    private function deleteAll($dir) {        
        
        $files = glob( $dir . '*', GLOB_MARK );
       
        foreach( $files as $file ){ 
            if( substr( $file, -1 ) == '/' ) 
                $this->deleteAll( $file ); 
            else 
                unlink( $file ); 
        } 
    
        if (is_dir($dir)) rmdir( $dir );    
    }
    
    public function addDeleteAction() {
        // Add delete 
        add_action('save_post', array($this, 'delete'));           
    }
    
    public function iptemp_cache_ob_callback($buffer) {
        
        if (!is_dir( $this->cache_path )) {
            if (!mkdir($this->cache_path, 0777, true)) {
                return 'Failed to create folders...' . $this->cache_path;
            }
        }        
        // Write to file
        file_put_contents($this->cache_file, $buffer . PHP_EOL . "<!-- Iphone Template Cache:
Engine: Disk
Caching: Enabled
Creation Time: " . round(microtime(true) - $this->cache_start, 4) . "
Created: " . date('r') . "
-->");

        // Write to output       
        return $buffer;
    }

    public function wphc_cache_shutdown_callback(){
        // Flush cache           
        ob_end_flush();        
        flush();        
    }

    public static function init() {        
        
        if (!isset(self::$instance)) {            
            $className = __CLASS__;
            self::$instance = new $className();
        }
        
        return self::$instance;
        
    }    
    
        
}
